{-# LANGUAGE RecordWildCards   #-}

module Main where

import Control.Monad
import GHC.Exts
import System.Environment

import GuessTree
import WordleUtils

----- CONSTANTS ----------------
wordsFile :: FilePath
wordsFile = "words.json"

----- MAIN ---------------------
main :: IO ()
main = do
  args <- getArgs
  wl@WordList{..} <- parseFile wordsFile
--  printTable $ sortByEntropy tmpList (solutions ++ herrings)
--  let splitGuess = fromString' $ head args
--      binnedWords = groupWith (\w -> checkAgainst w splitGuess) tmpList
  let myTree = makeFullTree solutions (solutions ++ herrings) -- CHECK AGAINST FULL LIST
--  let myTree = makeFullTree solutions solutions -- ONLY CHECK AGAINST POSSIBLE SOLUTIONS (faster)
  prettyPrintTree Nothing myTree
  putStrLn ""
  print $ maxDepth myTree
  print $ leafDepthHistogram myTree
