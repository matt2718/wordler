import Data.Aeson
import GHC.Generics

data ParsedWords
  = ParsedWords { solutions :: [String]
                , herrings  :: [String]
                }
    deriving (Show, Eq, Generic, FromJSON, ToJSON)

main = do
  putStrLn "hello"
