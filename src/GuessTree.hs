{-# LANGUAGE RecordWildCards   #-}

module GuessTree
  ( GuessTree(..)
  , expandTree
  , makeFullTree
  , prettyPrintTree
  , maxDepth
  , leafDepthHistogram
  ) where

import Control.Monad
import Data.List
import GHC.Exts
import Text.Printf

import WordleUtils

data GuessTree
  = Leaf Wordle             -- found solution
  | ComputedNode            -- set we've refined with the best guess
    { possibleSolutions :: [Wordle]
    , guess             ::  Wordle
    , children          :: [GuessTree]
    }
  | UncomputedNode [Wordle] -- set we haven't tried to refine
  | DeadNode [Wordle]       -- set that no guesses are able to refine
  deriving (Eq, Show)

-- | Gets the list of candidate solutions in a node, regardless of what node
-- type (mostly internal use)
candidateList :: GuessTree -> [Wordle]
candidateList (Leaf w)             = [w]
candidateList (ComputedNode l _ _) = l
candidateList (UncomputedNode l)   = l
candidateList (DeadNode l)         = l

----- TREE ASSEMBLY ------------

-- | Uses a given guess list to greedily expand a GuessTree, with an optional
-- number of levels to expand. The guess at each step is the word which
-- maximizes entropy over the list of remaining possible solutions.
expandTree :: [Wordle] -> Maybe Int -> GuessTree -> GuessTree

-- iteration cap:
expandTree _ (Just n) node | n <= 0  = node

-- stop on leaves and dead nodes:
expandTree _ _ node@(Leaf _) = node
expandTree _ _ node@(DeadNode _) = node

-- some nodes already have a best guess; recurse on children
expandTree guessList mbIters node@(ComputedNode _ _ children')
  = let recExpand = expandTree guessList (subtract 1 <$> mbIters)
    in node { children = map recExpand children' }

-- Single candidate solution that we've narrowed down to WITHOUT guessing
-- explicitly. If it's in our guess list, this becomes a ComputedNode with a
-- single leaf. If not, we're dead.
expandTree guessList mbIters (UncomputedNode [w])
  = if w `elem` guessList
    then ComputedNode [w] w [Leaf w]
    else DeadNode [w]

-- if we haven't run on this node, get best guess, split, and process children
expandTree guessList mbIters (UncomputedNode solList)
  = let possibleSolutions = solList
        -- max entropy:
        (guess, ent) = bestGuessEntropy solList guessList
--      -- HARD MODE:
--      (guess, ent) = bestGuessEntropy solList solList
--      -- JIGGLE TREE TO GET MAX DEPTH UNDER 6:
--      (guess', ent) = bestGuessEntropy solList guessList
--      guess = if guess' == fromString' "pigmy"
--              then fromString' "gawky"
--              else guess'

        -- split solutions:
        binnedWords = groupWith (\w -> checkAgainst w guess) solList
        children = (\wlist -> if head wlist == guess
                              then Leaf guess -- if we found answer
                              else UncomputedNode wlist)
                   <$> binnedWords
        -- now that we have a computed node, run normally on it:
    in
      if ent > 0
      then expandTree guessList mbIters ComputedNode{..}
      else DeadNode solList -- if we can't cut down the list, there's no hope

-- | Given a list of solutions and possible guesses, creates a fully expanded
-- GuessTree from scratch. The tree will contain dead nodes unless the solution
-- list is a subset of the guess list.
makeFullTree :: [Wordle] -> [Wordle] -> GuessTree
makeFullTree solutionList guessList
  = expandTree guessList Nothing (UncomputedNode solutionList)

----- PRINTING -----------------

-- | Given a maximum depth and a GuessTree, neatly prints the tree up to the
-- specified depth.
prettyPrintTree :: Maybe Int -> GuessTree -> IO()
prettyPrintTree = pptPrefix 2 0 ""

-- | Turns node into a (possibly colored) string that can be printed to the
-- terminal (internal use).
prettyShowNode :: GuessTree -> String
prettyShowNode (Leaf w)             = printf "\ESC[32m%s\ESC[39m" (toString w)
prettyShowNode (ComputedNode l w _) = printf "%s (%d)" (toString w) (length l)
prettyShowNode (UncomputedNode l)   = printf "\ESC[36m[UNCOMPUTED]\ESC[39m (%d)" (length l)
prettyShowNode (DeadNode l)         = printf "\ESC[31m[DEAD]\ESC[39m (%d)" (length l)

-- | Same as prettyPrintTree, but with a specified indentation step size,
-- initial whitespace offset, and prefix string (internal use).
pptPrefix :: Int -> Int -> String -> Maybe Int -> GuessTree -> IO ()

-- only print if depth is nonnegative
pptPrefix _ _ _ (Just n) _ | n < 0 = return ()

-- main case
pptPrefix indent offset prefix mbDepth node = do
  putStr   $ replicate offset ' '
  putStr   $ prefix
  putStrLn $ prettyShowNode node
  case node of
    Leaf _           -> return ()
    UncomputedNode _ -> return ()
    DeadNode _       -> return ()
    ComputedNode{..} ->
      if mbDepth == Just 0
      then putStrLn $ replicate newOffset ' ' ++ "..." --- end of depth
      else forM_ children $ -- recurse
           \child -> pptPrefix indent newOffset (getPrefix guess child)
                     newDepth child
  where
    newDepth = subtract 1 <$> mbDepth
    newOffset = indent + offset
    getPrefix guess child = colorAgainst (head $ candidateList child) guess ++ " -> "

----- TRAVERSAL ----------------

-- | Gets the maximum depth of any node in the tree. The root is defined to have
-- depth 0.
maxDepth :: GuessTree -> Int
maxDepth (Leaf _)           = 0
maxDepth (UncomputedNode _) = 0
maxDepth (DeadNode _)       = 0
maxDepth (ComputedNode _ _ children)
  = 1 + foldl1' max (map maxDepth children)

-- | Outputs a histogram containing the number of leaves at each depth
leafDepthHistogram :: GuessTree -> [Int]
leafDepthHistogram (Leaf _)           = [1]
leafDepthHistogram (UncomputedNode _) = []
leafDepthHistogram (DeadNode _)       = []
leafDepthHistogram (ComputedNode _ _ children)
  = 0 : foldl1' pairwiseSum (map leafDepthHistogram children)


pairwiseSum :: [Int] -> [Int] -> [Int]
pairwiseSum (x:xs) (y:ys) = x + y : pairwiseSum xs ys
pairwiseSum []     ys     = ys
pairwiseSum xs     []     = xs
