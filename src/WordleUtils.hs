{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE RecordWildCards   #-}

module WordleUtils
  ( CheckedResult(..)
  , Color(..)
  , FreqMap(..)
  , Wordle(..)
  , WordList(..)

  , fromString
  , fromString'
  , toString
  , checkAgainst
  , colorAgainst
  
  , mapIncrement
  , getFreq
  , freqBin
  , freqBin'
  , freqMapAssocs
  , listOfFreqs

  , parseFile
  , printTable

  , shannonBits
  , getEntropy
  , getEntropyNonuniform
  , sortByEntropy
  , bestGuessEntropy
  ) where

import Control.Monad
import Data.Maybe
import Data.List
import Data.Traversable
import GHC.Generics

import           Data.Aeson
import qualified Data.Map.Strict      as Map
import qualified Data.Vector          as Vec
import qualified Data.ByteString.Lazy as B
import           Linear.V

----- TYPES --------------------
type CheckedResult = V 5 Color

type FreqMap k n = Map.Map k n

data Color = Green | Yellow | Grey
  deriving (Show, Eq, Ord)

type Wordle = V 5 Char

data WordList
  = WordList { solutions :: [Wordle]
             , herrings  :: [Wordle]
             }
  deriving (Show, Eq)

-- internal
data RawWordList
  = RawWordList { jsonSolutions :: [String]
                , jsonHerrings  :: [String]
                }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

----- WORDLE UTILS -------------

-- | Converts a String to a Maybe Wordle; returns Nothing for the wrong length.
fromString :: String -> Maybe Wordle
fromString = fromVector . Vec.fromList

-- | Converts a String to a Wordle; fails for the the wrong length.
fromString' :: String -> Wordle
fromString' = fromJust . fromString

-- | Converts a Wordle to a String.
toString :: Wordle -> String
toString = Vec.toList . toVector

-- | Checks a guess against a solution, returning a vector of Colors.
checkAgainst :: Wordle -> Wordle -> CheckedResult
checkAgainst solution guess =
  snd $ mapAccumL getColor unmatchedChars zipped
  where
    zipped = (,) <$> solution <*> guess
    unmatchedChars = foldr (\(x,y) -> if x == y then id else mapIncrement 1 x)
                     Map.empty zipped
    getColor freqMap (x,y) | x == y                = (freqMap, Green)
                           | getFreq y freqMap > 0 = (mapIncrement (-1) y freqMap,
                                                      Yellow)
                           | otherwise             = (freqMap, Grey)

----- COLOR / PRETTY PRINTING --

-- | Checks a guess against a given solution and returns a string of
-- ANSI color codes.
colorAgainst :: Wordle -> Wordle -> String
colorAgainst solution guess =
  foldr (++) resetANSI coloredLetters
  where
    colorStrings  = toANSI <$> checkAgainst solution guess
    letterStrings = (\x -> [x]) <$> guess
    coloredLetters = (++) <$> colorStrings <*> letterStrings

toANSI :: Color -> String
toANSI Green  = "\ESC[32m"
toANSI Yellow = "\ESC[33m"
toANSI Grey   = resetANSI

resetANSI :: String
resetANSI = "\ESC[39m"

----- FREQUENCY MAP UTILS ------
mapIncrement :: (Ord k, Num n) => n -> k -> FreqMap k n -> FreqMap k n
mapIncrement i key = Map.insertWith (+) key i

getFreq :: (Ord k, Num n) => k -> FreqMap k n -> n
getFreq = Map.findWithDefault 0

freqBin :: (Ord k, Num n) => [(k, n)] -> FreqMap k n
freqBin = foldr (uncurry $ Map.insertWith (+)) Map.empty

freqBin' :: (Ord k) => [k] -> FreqMap k Int
freqBin' = foldr (mapIncrement 1) Map.empty

freqMapAssocs :: (Ord k, Ord n, Num n) => FreqMap k n -> [(k,n)]
freqMapAssocs = filter ((>0) . snd) . Map.assocs

listOfFreqs :: (Ord k, Ord n, Num n) => FreqMap k n -> [n]
listOfFreqs = filter (>0) . Map.elems

----- IO / MISC UTILS ----------

-- | Reads a file into a WordList
parseFile :: FilePath -> IO (WordList)
parseFile path = do
  bytes <- B.readFile path
  case (eitherDecode bytes) of
    Left errorMessage     -> ioError (userError errorMessage)
    Right RawWordList{..} -> return $ WordList
      { solutions = (fromString <$> jsonSolutions) >>= maybeToList
      , herrings  = (fromString <$> jsonHerrings ) >>= maybeToList
      }

-- | Given a list of Wordles and numbers, prints the wordles
printTable :: [(Wordle, Double)] -> IO ()
printTable fList =
  forM_ fList (\(w,f) -> putStrLn $ (toString w) ++ "\t" ++ (show f))

----- INFORMATION ANALYSIS -----

-- | Calculates the partial Shannon entropy, in bits, of an event with
-- probability p
shannonBits :: Double -> Double
shannonBits 0 = 0
shannonBits p = -p * logBase 2 p

-- | Calculates the entropy (in bits) of a guess against a list of equally
-- likely possible solutions.
getEntropy :: [Wordle] -> Wordle -> Double
getEntropy = getEntropyNonuniform . map (\x -> (x,1))

-- | Calculates the entropy of a guess against a list of possible solutions and
-- their relative frequencies. The frequencies need not be normalized.
getEntropyNonuniform :: [(Wordle, Double)] -> Wordle -> Double
getEntropyNonuniform solutionFreqList word
  = let fList = listOfFreqs . freqBin
          $ map (\(s,f) -> (checkAgainst s word, f)) solutionFreqList
        total = foldl1' (+) fList
        entropies = map (\x -> shannonBits (x / total)) fList
    in foldl1' (+) entropies

-- | For a list of equally likely solutions and a list of guesses, ranks the
-- guesses by their informational content, returning an ascending list of
-- guesses and their entropies (in bits).
sortByEntropy :: [Wordle] -> [Wordle] -> [(Wordle, Double)]
sortByEntropy solutionList guessList
  = sortOn snd $ map (\w -> (w, getEntropy solutionList w)) guessList

-- | Returns the highest-entropy guess, and its entropy, against a list of
-- solutions (mostly for convenience).
bestGuessEntropy :: [Wordle] -> [Wordle] -> (Wordle, Double)
bestGuessEntropy solutionList guessList
  -- if entropies are equal, try for early escape
  = let takeMax a b = if (snd a > snd b) 
                         || (snd a == snd b && fst a `elem` solutionList)
--                       || (fst a == fromString' "raise") -- force first word
                      then a else b
    in foldl1' takeMax $ map (\w -> (w, getEntropy solutionList w)) guessList
