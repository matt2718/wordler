# wordler

Calculates a close-to-optimal tree of Wordle guesses by locally maximizing
Shannon entropy.
